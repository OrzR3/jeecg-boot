package org.jeecg.modules.demo.myTestDemo.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.demo.myTestDemo.entity.MyTestDemo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 我的测试demo
 * @Author: jeecg-boot
 * @Date:   2024-08-09
 * @Version: V1.0
 */
public interface IMyTestDemoService extends IService<MyTestDemo> {
    List<Map<String,Object>> findVisitCount();

    void updateTest(JSONObject jsonObject);
}
