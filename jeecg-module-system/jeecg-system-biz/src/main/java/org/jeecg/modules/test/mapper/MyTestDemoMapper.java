package org.jeecg.modules.demo.myTestDemo.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.myTestDemo.entity.MyTestDemo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 我的测试demo
 * @Author: jeecg-boot
 * @Date:   2024-08-09
 * @Version: V1.0
 */
public interface MyTestDemoMapper extends BaseMapper<MyTestDemo> {
    List<Map<String,Object>> findVisitCount();

    void updateTest(JSONObject jsonObject);
}
