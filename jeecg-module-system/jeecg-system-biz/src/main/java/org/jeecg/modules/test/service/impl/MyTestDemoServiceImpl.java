package org.jeecg.modules.demo.myTestDemo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.DbType;
import org.jeecg.common.util.CommonUtils;
import org.jeecg.modules.demo.myTestDemo.entity.MyTestDemo;
import org.jeecg.modules.demo.myTestDemo.mapper.MyTestDemoMapper;
import org.jeecg.modules.demo.myTestDemo.service.IMyTestDemoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 我的测试demo
 * @Author: jeecg-boot
 * @Date:   2024-08-09
 * @Version: V1.0
 */
@Service
public class MyTestDemoServiceImpl extends ServiceImpl<MyTestDemoMapper, MyTestDemo> implements IMyTestDemoService {

    @Resource
    private  MyTestDemoMapper myTestDemoMapper;

    @Override
    public List<Map<String,Object>> findVisitCount() {
        return myTestDemoMapper.findVisitCount();
    }

    @Override
    public void updateTest(JSONObject jsonObject){
        myTestDemoMapper.updateTest(jsonObject);
    }
}
