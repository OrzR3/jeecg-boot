package org.jeecg.modules.demo.myTestDemo.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.myTestDemo.entity.MyTestDemo;
import org.jeecg.modules.demo.myTestDemo.service.IMyTestDemoService;
import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 我的测试demo
 * @Author: jeecg-boot
 * @Date:   2024-08-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags="我的测试demo")
@RestController
@RequestMapping("/myTestDemo")
public class MyTestDemoController extends JeecgController<MyTestDemo, IMyTestDemoService> {
	@Autowired
	private IMyTestDemoService myTestDemoService;

 	@AutoLog(value = "我的测试demo-测试列表")
 	@ApiOperation(value="我的测试demo-测试列表", notes="测试列表")
	@GetMapping(value = "/getList")
	public Result<?> getList(MyTestDemo myTestDemo, HttpServletRequest req){
		log.info("xxxxxxxxxxxxxx");
		JSONArray data = new JSONArray();
//		        List<Map<String,Object>> list = logService.findVisitCount(dayStart, dayEnd);
		List<Map<String, Object>> list = myTestDemoService.findVisitCount();
		return Result.OK(list);
	}

     /**
      * 编辑
      *
      * @param jsonObject
      * @return
      */
	 @AutoLog(value = "updateTest-编辑")
	 @ApiOperation(value="updateTest-编辑", notes="updateTest-编辑")
     @RequestMapping(value = "/updateTest", method = {RequestMethod.POST,RequestMethod.PUT})
     public Result<?> updateTest(@RequestParam("jsonData") String jsonData) {
		 // 处理 jsonData 和 file
		 JSONObject jsonObject = JSONObject.parseObject(jsonData);
         myTestDemoService.updateTest(jsonObject);
         return Result.OK("编辑成功!");
     }
	/**
	 * 分页列表查询
	 *
	 * @param myTestDemo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "我的测试demo-分页列表查询")
	@ApiOperation(value="我的测试demo-分页列表查询", notes="我的测试demo-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MyTestDemo myTestDemo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MyTestDemo> queryWrapper = QueryGenerator.initQueryWrapper(myTestDemo, req.getParameterMap());
		Page<MyTestDemo> page = new Page<MyTestDemo>(pageNo, pageSize);
		IPage<MyTestDemo> pageList = myTestDemoService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param myTestDemo
	 * @return
	 */
	@AutoLog(value = "我的测试demo-添加")
	@ApiOperation(value="我的测试demo-添加", notes="我的测试demo-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MyTestDemo myTestDemo) {
		myTestDemoService.save(myTestDemo);
		return Result.OK("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param myTestDemo
	 * @return
	 */
	@AutoLog(value = "我的测试demo-编辑")
	@ApiOperation(value="我的测试demo-编辑", notes="我的测试demo-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody MyTestDemo myTestDemo) {
		myTestDemoService.updateById(myTestDemo);
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "我的测试demo-通过id删除")
	@ApiOperation(value="我的测试demo-通过id删除", notes="我的测试demo-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		myTestDemoService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "我的测试demo-批量删除")
	@ApiOperation(value="我的测试demo-批量删除", notes="我的测试demo-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.myTestDemoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "我的测试demo-通过id查询")
	@ApiOperation(value="我的测试demo-通过id查询", notes="我的测试demo-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MyTestDemo myTestDemo = myTestDemoService.getById(id);
		return Result.OK(myTestDemo);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param myTestDemo
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, MyTestDemo myTestDemo) {
      return super.exportXls(request, myTestDemo, MyTestDemo.class, "我的测试demo");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, MyTestDemo.class);
  }

}
